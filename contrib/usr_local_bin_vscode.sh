#!/bin/sh
docker start vscode 2> /dev/null || \
docker run -d \
   -v "$HOME":/home/tom \
   -v /var/run/docker.sock:/var/run/docker.sock \
   -v /tmp/.X11-unix:/tmp/.X11-unix \
   -e DISPLAY=unix"$DISPLAY" \
   --device /dev/dri \
   --name vscode \
   --hostname vscode \
   letompouce/vscode:latest
